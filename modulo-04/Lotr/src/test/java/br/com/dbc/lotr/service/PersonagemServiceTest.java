/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.service;

import br.com.dbc.lotr.dto.PersonagemDTO;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.PersonagemJoin;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.Usuario;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author guilherme.borges
 */
public class PersonagemServiceTest {
    
    public PersonagemServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of salvarPersonagem method, of class PersonagemService.
     */
    @Test
    public void testSalvarPersonagem() {
        Session session = HibernateUtil.getSession();
        HibernateUtil.beginTransaction();        
        
        System.out.println("salvarPersonagem");
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setNome("Legolas");
        personagemDTO.setDanoElfo(100d);
        
        
        Usuario usuario = new Usuario();
        usuario.setApelido("Teste");
        usuario.setNome("Guilherme");
        usuario.setCpf(123l);
        usuario.setSenha("123");
        
        session.save(usuario);
        session.getTransaction().commit();
        
        PersonagemService instance = new PersonagemService();
        instance.salvarPersonagem(personagemDTO, usuario);
        
        
        final List personagens = session.createCriteria(PersonagemJoin.class)
                .list();
        
        Assert.assertEquals(1, personagens.size());
        
        
        Criteria criteria = session.createCriteria(Usuario.class);
        criteria.add(Restrictions.ilike("apelido", "Teste"));
        Usuario teste = (Usuario) criteria.uniqueResult();
        Assert.assertEquals("Teste", teste.getApelido());
    }
    
}
