/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.PersonagemDTO;

/**
 *
 * @author guilherme.borges
 */
public class HobbitDAO extends AbstractDAO<HobbitJoin>{

    @Override
    protected Class<HobbitJoin> getEntityClass() {
        return HobbitJoin.class;
    }
    
    public HobbitJoin parseFrom(PersonagemDTO dto, Usuario usuario) {
        HobbitJoin hobbitJoin = null;
        if(dto.getId() != null) {
            hobbitJoin = buscar(dto.getId());
        }
        if( hobbitJoin == null ) {
            hobbitJoin = new HobbitJoin();
        }
        hobbitJoin.setNome(dto.getNome());
        hobbitJoin.setDanoHobbit(dto.getDanoHobbit());
        hobbitJoin.setUsuario(usuario);
        
        return hobbitJoin;
    }
    
    
}
