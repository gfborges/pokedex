/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author guilherme.borges
 */
@Entity
@Table(name = "PERSONAGEM_JOIN")
@Inheritance(strategy = InheritanceType.JOINED)
public class PersonagemJoin extends AbstractEntity{
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PERSONAGEM_JOIN_SEQ", sequenceName = "PERSONAGEM_JOIN_SEQ")
    @GeneratedValue(generator = "PERSONAGEM_JOIN_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String nome;
    
    @Enumerated(EnumType.STRING)
    private RacaType raca;
    
    @ManyToOne
    @JoinColumn(name = "ID_USUARIO")
    private Usuario usuario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public RacaType getRaca() {
        return raca;
    }

    protected void setRaca(RacaType raca) {
        this.raca = raca;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    } 
    
    
}
