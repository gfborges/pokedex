/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.cartoes.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author guilherme.borges
 */
@Entity
@Table(name = "LOJA_CREDENCIADOR")
public class LojaCredenciador {
    
    @EmbeddedId
    private LojaCredenciadorId id;
    
    @ManyToOne
    @JoinColumn(name = "ID_LOJA")
    private Loja loja;

    @ManyToOne
    @JoinColumn(name = "ID_CREDENCIADOR")
    private Credenciador credenciador;
    
    private Double taxa;

    public LojaCredenciadorId getId() {
        return id;
    }

    public void setId(LojaCredenciadorId id) {
        this.id = id;
    }

    public Double getTaxa() {
        return taxa;
    }

    public void setTaxa(Double taxa) {
        this.taxa = taxa;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public Credenciador getCredenciador() {
        return credenciador;
    }

    public void setCredenciador(Credenciador credenciador) {
        this.credenciador = credenciador;
    }
    
    
    
    
    
}
