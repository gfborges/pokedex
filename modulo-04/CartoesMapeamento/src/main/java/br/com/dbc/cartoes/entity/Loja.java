/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.cartoes.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author guilherme.borges
 */
@Entity
@Table(name = "LOJA")
public class Loja {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "loja_seq", sequenceName = "loja_seq")
    @GeneratedValue(generator = "loja_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String nome;
    
    @OneToMany(mappedBy = "loja", cascade = CascadeType.ALL)
    private List<LojaCredenciador> lojaCredenciadores = new ArrayList<>();
    
    @OneToMany(mappedBy = "loja", cascade = CascadeType.ALL)
    private List<Lancamento> lancamentos = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<LojaCredenciador> getLojaCredenciadores() {
        return lojaCredenciadores;
    }

    public void pushLojaCredenciadores(LojaCredenciador... lojaCredenciadores) {
        this.lojaCredenciadores.addAll(Arrays.asList(lojaCredenciadores));
    }
    
    public List<Lancamento> getLancamentos() {
        return lancamentos;
    }

    public void pushLancamentos(Lancamento... lancamentos) {
        this.lancamentos.addAll(Arrays.asList(lancamentos));
    }
}
