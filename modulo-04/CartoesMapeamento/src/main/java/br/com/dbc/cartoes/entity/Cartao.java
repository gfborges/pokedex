/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.cartoes.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author guilherme.borges
 */
@Entity
@Table(name = "CARTAO")
public class Cartao {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "cartao_seq", sequenceName = "cartao_seq")
    @GeneratedValue(generator = "cartao_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String chip;
    
    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE")
    private Cliente cliente;
    
    @ManyToOne
    @JoinColumn(name = "ID_BANDEIRA")
    private Bandeira bandeira;
    
    @ManyToOne
    @JoinColumn(name = "ID_EMISSOR")
    private Emissor emissor;
    
    private String vencimento;
    
    @OneToMany(mappedBy = "cartao", cascade = CascadeType.ALL)
    private List<Lancamento> lancamentos = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChip() {
        return chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    public String getVencimento() {
        return vencimento;
    }

    public void setVencimento(String vencimento) {
        this.vencimento = vencimento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Bandeira getBandeira() {
        return bandeira;
    }

    public void setBandeira(Bandeira bandeira) {
        this.bandeira = bandeira;
    }

    public Emissor getEmissor() {
        return emissor;
    }

    public void setEmissor(Emissor emissor) {
        this.emissor = emissor;
    }   
    
    public List<Lancamento> getLancamentos() {
        return lancamentos;
    }

    public void pushLancamentos(Lancamento... lancamentos) {
        this.lancamentos.addAll(Arrays.asList(lancamentos));
    }
}
