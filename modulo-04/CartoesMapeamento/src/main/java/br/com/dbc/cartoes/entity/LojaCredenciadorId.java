/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.cartoes.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author guilherme.borges
 */
@Embeddable
public class LojaCredenciadorId {
 
    @Column(name = "ID_LOJA")
    private Integer idLoja;
    
    @Column(name = "ID_CREDENCIADOR")
    private Integer idCredenciador;

    
    public LojaCredenciadorId(){
    }
    
    public LojaCredenciadorId(Integer idLoja, Integer idCredenciador) {
        this.idLoja = idLoja;
        this.idCredenciador = idCredenciador;
    }

    public Integer getIdLoja() {
        return idLoja;
    }

    public Integer getIdCredenciador() {
        return idCredenciador;
    }
    
    
}
