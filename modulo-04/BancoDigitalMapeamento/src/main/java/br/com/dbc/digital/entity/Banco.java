/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.digital.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author guilherme.borges
 */
@Entity
@Table(name = "BANCO")
public class Banco {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "banco_seq", sequenceName = "banco_seq")
    @GeneratedValue(generator = "banco_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
           
    private String Nome;
    
    private Integer codigo;
    
    @OneToMany(mappedBy = "banco", cascade = CascadeType.ALL)
    private List<Agencia> agencias = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public List<Agencia> getAgencias() {
        return agencias;
    }

    public void pushAgencias(Agencia... agencias) {
        this.agencias.addAll(Arrays.asList(agencias));
    }
    
    
    
}
