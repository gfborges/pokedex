/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.digital.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "AGENCIA")
public class Agencia {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "agencia_seq", sequenceName = "agencia_seq")
    @GeneratedValue(generator = "agencia_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "ID_BANCO")
    private Banco banco;
    
    private String nome;
    
    private Integer codigo;
    
    @OneToMany(mappedBy = "agencia", cascade = CascadeType.ALL)
    private List<Conta> contas = new ArrayList<>();
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public List<Conta> getContas() {
        return contas;
    }

    public void pushContas(Conta... contas) {
        this.contas.addAll(Arrays.asList(contas));
    }
    
    
    
}
