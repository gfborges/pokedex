package br.com.topsafe.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "SERVICO")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Servico {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "SERVICO_SEQ", sequenceName = "SERVICO_SEQ")
	@GeneratedValue(generator = "SERVICO_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(nullable = false)
	private String nome;
	
	@Column(nullable = false)
	private String descricao;
	
	@Column(nullable = false, name = "VALOR_PADRAO")
	private Double valorPadrao;
	
	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(name = "SEGURADORA_SERVICO",
			joinColumns = { @JoinColumn(name = "ID_SERVICO")},
			inverseJoinColumns = { @JoinColumn(name = "ID_SEGURADORA")})
	private List<Seguradora> seguradoras = new ArrayList<>();
	
	@OneToOne(mappedBy = "servico")
	private ServicoContratado servicoContratado;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValorPadrao() {
		return valorPadrao;
	}

	public void setValorPadrao(Double valorPadrao) {
		this.valorPadrao = valorPadrao;
	}

	public List<Seguradora> getSeguradoras() {
		return seguradoras;
	}

	public void pushSeguradoras(Seguradora... seguradoras) {
		this.seguradoras.addAll(Arrays.asList(seguradoras));
	}

	public ServicoContratado getServicoContratado() {
		return servicoContratado;
	}

	public void setServicoContratado(ServicoContratado servicoContratado) {
		this.servicoContratado = servicoContratado;
	}
		

}
