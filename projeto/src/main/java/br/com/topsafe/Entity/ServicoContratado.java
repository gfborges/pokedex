package br.com.topsafe.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "SERVICO_CONTRATADO")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ServicoContratado {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "SERVICO_CONTRATADO_SEQ", sequenceName = "SERVICO_CONTRATADO_SEQ")
	@GeneratedValue(generator = "SERVICO_CONTRATADO_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(nullable = false)
	private String descricao;
	
	@Column(nullable = false)
	private Double valor;
	
	@OneToOne
	@JoinColumn(name = "ID_SERVICO")
	private Servico servico;
	
	@OneToOne
	@JoinColumn(name = "ID_SEGURADORA")
	private Seguradora seguradora;
	
	@OneToOne
	@JoinColumn(name = "ID_PESSOA_SEGURADO")
	private Segurado segurado;
	
	@OneToOne
	@JoinColumn(name = "ID_PESSOA_CORRETOR")
	private Corretor corretor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}

	public Segurado getSegurado() {
		return segurado;
	}

	public void setSegurado(Segurado segurado) {
		this.segurado = segurado;
	}

	public Corretor getCorretor() {
		return corretor;
	}

	public void setCorretor(Corretor corretor) {
		this.corretor = corretor;
	}
	

}
