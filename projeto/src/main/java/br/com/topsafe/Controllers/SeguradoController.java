package br.com.topsafe.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.topsafe.Entity.Segurado;
import br.com.topsafe.Services.SeguradoService;

@Controller
@RequestMapping("/api/segurado")
public class SeguradoController {
	
	@Autowired
	SeguradoService seguradoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Segurado> listarTodos() {
		return seguradoService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Segurado buscarPorID(@PathVariable Integer id) {
		return seguradoService.buscarPorID(id);		
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Segurado salvarNovo(@RequestBody Segurado segurado) {
		return seguradoService.salvar(segurado);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Segurado editar(@PathVariable Integer id, @RequestBody Segurado segurado) {
		return seguradoService.editar(id, segurado);
	}
}
