package br.com.topsafe.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.topsafe.Entity.Endereco;
import br.com.topsafe.Services.EnderecoService;

@Controller
@RequestMapping("/api/endereco")
public class EnderecoController {
	
	@Autowired
	EnderecoService enderecoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Endereco> listarTodos() {
		return enderecoService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Endereco buscarPorID(@PathVariable Integer id) {
		return enderecoService.buscarPorID(id);		
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Endereco salvarNovo(@RequestBody Endereco endereco) {
		return enderecoService.salvar(endereco);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Endereco editar(@PathVariable Integer id, @RequestBody Endereco endereco) {
		return enderecoService.editar(id, endereco);
	}
}
