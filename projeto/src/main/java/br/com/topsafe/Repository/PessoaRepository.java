package br.com.topsafe.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.topsafe.Entity.Pessoa;

public interface PessoaRepository<E extends Pessoa> extends CrudRepository<E, Integer>{
	E findByNome(String nome);
	E findByCpf(String cpf);
	E findByPai(String pai);
	E findByMae(String mae);
	E findByTelefone(String telefone);
	E findByEmail(String email);	
}
