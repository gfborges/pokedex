package br.com.topsafe.Repository;

import java.util.List;

import br.com.topsafe.Entity.Corretor;

public interface CorretorRepository extends PessoaRepository<Corretor>{
	List<Corretor> findAllByCargo(String cargo);
	List<Corretor> findAllByComissao(Double comissao);
}
