package br.com.topsafe.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.topsafe.Entity.EnderecoSeguradora;
import br.com.topsafe.Entity.Seguradora;
import br.com.topsafe.Entity.ServicoContratado;

public interface SeguradoraRepository extends CrudRepository<Seguradora, Integer>{
	Seguradora findByNome(String nome);
	Seguradora findByCnpj(String cnpj);
	Seguradora findByEnderecoSeguradora(EnderecoSeguradora endereco);
	Seguradora findByServicoContratado(ServicoContratado servico);
}
