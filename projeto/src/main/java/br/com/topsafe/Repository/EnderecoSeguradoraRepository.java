package br.com.topsafe.Repository;

import br.com.topsafe.Entity.EnderecoSeguradora;
import br.com.topsafe.Entity.Seguradora;

public interface EnderecoSeguradoraRepository extends EnderecoRepository<EnderecoSeguradora>{
	EnderecoSeguradora findBySeguradora(Seguradora seguradora);
}
