package br.com.topsafe.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.topsafe.Entity.Servico;
import br.com.topsafe.Entity.ServicoContratado;

public interface ServicoRepository extends CrudRepository<Servico, Integer>{
	List<Servico> findAllByNome(String nome);
	List<Servico> findAllByDescricao(String descricao);
	List<Servico> findAllByValorPadrao(Double valor);
	Servico findByServicoContratado(ServicoContratado servico);
}
