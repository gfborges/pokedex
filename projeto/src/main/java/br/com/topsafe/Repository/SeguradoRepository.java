package br.com.topsafe.Repository;

import java.util.List;

import br.com.topsafe.Entity.Segurado;

public interface SeguradoRepository extends PessoaRepository<Segurado>{
	List<Segurado> findAllByQtdServicos(Integer qtdServicos);
}
